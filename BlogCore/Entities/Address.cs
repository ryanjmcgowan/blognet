﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogCore.Entities
{
    public class Address : BaseContent
    {
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool IsPublic { get; set; }
        public bool IsMailing { get; set; }
        public bool IsPhysical { get; set; }

        public Address()
        {
            IsPublic = false;
            IsMailing = false;
            IsPhysical = false;
            Street1 = "";
            Street2 = "";
            City = "";
            State = "";
            Zip = "";
        }
    }
}