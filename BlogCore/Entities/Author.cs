﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlogCore.Entities
{
    public class Author : BaseContent
    {
        public string Name { get; set; }
        public List<ContactInfo> ContactInfo { get; set; }
        public string UserName { get; set; }
        public int primaryContactID { get; set; }

        public Author()
        {
            ContactInfo = new List<ContactInfo>();
        }

        public Author(List<ContactInfo> contactInfo)
        {
            this.ContactInfo = contactInfo;
        }

        public override string ToString()
        {
            return "ID: " + ID
                + "Name: " + Name
                + " Created: " + base.Created
                + " Modified : " + base.LastModified;
        }

        public void SetPrimaryContactInfo(ContactInfo contactInfo)
        {
            primaryContactID = contactInfo.ID;
            if (!ContactInfo.Contains(contactInfo))
            {
                ContactInfo.Add(contactInfo);
            }
            
        }

        public ContactInfo GetPrimaryContact()
        {
            return ContactInfo.Where(c => c.ID == primaryContactID).FirstOrDefault();
        }
    }
}
