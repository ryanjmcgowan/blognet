﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogCore.Entities
{
    public abstract class BaseContent : BaseEntity
    {
        internal DateTime created;
        private DateTime lastModified;

        public DateTime Created { get { return created; } }
        public DateTime LastModified { get { return lastModified; } }

        public BaseContent()
        {
            created = DateTime.Now.ToUniversalTime();
            lastModified = DateTime.Now.ToUniversalTime();
        }

        public BaseContent(int id, DateTime created, DateTime lastModified)
        {
            base.ID = id;
            this.created = created;
            this.lastModified = lastModified;
        }
    }
}
