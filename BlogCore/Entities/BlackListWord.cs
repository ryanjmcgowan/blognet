﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogCore.Entities
{
    public class BlackListWord
    {
        public int ID { get; set; }
        public string Word { get; set; }
    }
}