﻿using System.Collections.Generic;

namespace BlogCore.Entities
{
    public class Category : BaseContent
    {
        public string Name { get; set; }
        public int Order { get; set; }
        public bool IsDeleted { get; set; }
    }
}