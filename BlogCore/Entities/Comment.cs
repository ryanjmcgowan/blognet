﻿using System;
using System.Collections.Generic;

namespace BlogCore.Entities
{
    public class Comment : BaseContent
    {
        public Post Post { get; set; }
        public string CommentText { get; set; }
        public List<SpamFlag> Flags { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool IsApproved { get; set; }
        public bool IsSpam { get; set; }
        public bool IsMarkedAsSpam { get; set; }
        public bool IsReply { get; set; }
        public IUser User { get; set; }
        public Comment ReplyTo { get; set; }
        public int Version { get; set; }
    }
}