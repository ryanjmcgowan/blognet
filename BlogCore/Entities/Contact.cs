﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogCore.Entities
{
    public class ContactInfo : BaseContent
    {
        public ContactType ContactType { get; set; }
        public Address Address { get; set; }
        public Phone PrimaryPhoneNumber { get; set; }
        public Phone SecondaryPhoneNumber { get; set; }//TODO: Only one Phone per contact info.
        public Email Email { get; set; }
        public string TwitterCallsign { get; set; }//TODO: Move this stuff into a separate Social class.
        public string FacebookCallsign { get; set; }
        public string InstagramUserName { get; set; }
        public string LinkedInUserName { get; set; }
        public string PintrestUserName { get; set; }
        public string GooglePlusUserName { get; set; }
        public string MySpaceUserName { get; set; }
        public string MeetupUserName { get; set; }

        public ContactInfo()
        {
            ContactType = Entities.ContactType.Unknown;
            Address = new Address();
            Email = new Email("");
            PrimaryPhoneNumber = new Phone("");
            SecondaryPhoneNumber = new Phone("");
        }

        public ContactInfo(string email, string phone, Address address, ContactType contactType)
        {
            Email = new Email(email);
            Address = address;
            ContactType = contactType;
        }

    }

    public enum ContactType
    {
        Home,
        Office,
        Employer,
        Other,
        Unknown
    }
}