﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogCore.Entities
{
    public class Email : BaseContent
    {
        private bool isValid;

        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
        public bool IsValid { get { return isValid; } }
        public bool IsSubscribed { get; set; }
        public bool IsDoNotContact { get; set; }
        public ContactInfo Contact { get; set; }

        public Email()
        {
            EmailAddress = "";
            DisplayName = "";
            isValid = false;
            IsSubscribed = false;
            IsDoNotContact = false;
        }
        public Email(string email)
        {
            this.EmailAddress = email;
            this.isValid = validate();
            this.IsSubscribed = false;
            this.IsDoNotContact = false;
        }

        private bool validate()
        {
            //TODO:
            return false;
        }
    }
    public enum EmailType
    {
        Work,
        Personal,
        Unknown,
        Other
    }
}