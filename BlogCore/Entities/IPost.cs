﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlogCore.Entities
{
    public interface IPost
    {
        string Title { get; set; }
        string Subheading { get; set; }
        string Content { get; set; }
        string FeatureImageKey { get; set; }
        bool IsVisible { get; set; }
    }
}
