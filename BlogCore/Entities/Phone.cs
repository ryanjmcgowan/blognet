﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogCore.Entities
{
    public class Phone : BaseContent
    {
        public string Number { get; set; }
        public string CountryCode { get; set; }
        public string Extension { get; set; }
        public bool IsSMS { get; set; }
        public bool IsDoNotCall { get; set; }
        public bool IsDoNotSMS { get; set; }

        public Phone(string number)
        {
            
            Number = number;
            Validate();
        }

        public Phone(string countryCode, string number)
        {
            Number = number;
            CountryCode = countryCode;
            Validate();
        }

        public Phone(string countryCode, string number, string extension)
        {
            Number = number;
            CountryCode = countryCode;
            Extension = extension;
            Validate();
        }

        public override string ToString()
        {
            return CountryCode + Number + Extension; 
        }
        private void Validate()
        {
            //TODO: throw exception for non-numerics, too long, whatever else.
        }

    }
}