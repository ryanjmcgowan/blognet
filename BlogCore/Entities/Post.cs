﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace BlogCore.Entities
{
    public class Post : BaseContent, IPost
    {
        // private fields
        private DateTime? publishDate;

        // properties
        public string Title { get; set; }
        public string Subheading { get; set; }
        public string URL { get; set; }
        public string Content { get; set; }

        //TODO: Make framework for uploading image, and generating key for URL.
        //Append ".jpg" and precede with folder to get image URL.
        [Required]
        [Display(Name = "Feature Image")]
        public string FeatureImageKey { get; set; }

        public List<UniqueView> UniqueViews { get; set; }

        [Display(Name = "Visible")]
        public bool IsVisible { get; set; }

        [JsonIgnore]
        public List<Comment> Comments { get; set; }
        [JsonIgnore]
        public List<Category> Categories { get; set; }
        [JsonIgnore]
        public Author Author { get; set; }
        [JsonIgnore]
        public List<Tag> Tags { get; set; }
        [JsonIgnore]
        public Schedule Schedule { get; set; }
        [JsonIgnore]
        public List<UpVote> UpVotes { get; set; }

        // constructors
        public Post()
        {
            Initialize(null, null, null, false, -1);
        }

        public Post(Author author, Schedule schedule)
        {
            Author = author;
            Schedule = schedule;
            Initialize("", "", "", false, -1);
        }
        public Post(string heading, string content, int authorID)
        {
            Initialize(heading, content, null, false, authorID);
        }

        public Post(string heading, string content, string featureImageURL, int authorID)
        {
            Initialize(heading, content, featureImageURL, false, authorID);
        }

        public Post(string heading, string content, string featureImageURL, bool isVisible, int authorID)
        {
            Initialize(heading, content, featureImageURL, isVisible, authorID);
        }

        // public methods
        public void Publish(DateTime date)
        {
            publishDate = date;
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            URL =
                  publishDate.Value.Year.ToString() + "/"
                + publishDate.Value.Month.ToString() + "/"
                + textInfo.ToTitleCase(Title.Trim().Replace(" ", ""));
        }

        //Initialize
        private void Initialize(string title, string content, string featureImageKey, bool isVisible, int authorID)
        {
            Title = title;
            Content = content;
            FeatureImageKey = featureImageKey;
            UniqueViews = new List<UniqueView>();
            IsVisible = isVisible;
            Author = new Author() { ID = authorID };
        }
    }
}
