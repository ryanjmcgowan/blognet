﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlogCore.Entities
{
    public class Schedule :BaseContent
    {
        [Display(Name = "Publish Date")]
        public DateTime PublishDate { get; set; }
        public bool ShareOnTwitter {get;set;}
        public bool ShareOnFacebook { get; set; }
        public bool ShareOnInstagram { get; set; }
        public bool ShareOnLinkedIn { get; set; }
        public bool ShareOnPintrest { get; set; }
        public bool ShareOnGooglePlus { get; set; }
        public bool ShareOnMySpace { get; set; }
        public bool ShareOnMeetup { get; set; }

        public Schedule()
        {
            PublishDate = new DateTime(2100, 1, 1);
        }
    }
}