﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlogCore.Entities
{
    public class SmartList<T> : List<T>
    {
        private int primaryIndex = 0;

        public event EventHandler OnAdd;

        public void Add(T item)
        {
            if (null != OnAdd)
            {
                OnAdd(item, null);
            }
            base.Add(item);
        }

        public T GetPrimary()
        {
            return (T)this.ElementAtOrDefault(primaryIndex);
        }

        /// <summary>
        /// If the object isn't in the list, adds it to the list. Marks it as Primary.
        /// </summary>
        /// <param name="record"></param>
        public void MakePrimary(T record)
        {
            if (!this.Contains(record))
            {
                this.Add(record);
            }
            primaryIndex = this.IndexOf(record);
        }

        public void MakePrimary(Func<T, bool> predicate)
        {
            //TODO: Need to test out functionality of SmartList<T>.MakePrimary(Func<T, bool> predicate)
            T target = Enumerable.Where(this,predicate).FirstOrDefault();
            primaryIndex = this.IndexOf(target);
        }
    }
}