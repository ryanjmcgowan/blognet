﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogCore.Entities
{
    public class SpamFlag : BaseContent
    {
        public IUser User { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}