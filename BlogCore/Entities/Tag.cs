﻿using System.Collections.Generic;

namespace BlogCore.Entities
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }
    }
}