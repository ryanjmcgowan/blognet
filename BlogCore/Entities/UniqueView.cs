﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogCore.Entities
{
    public class UniqueView
    {
        //TODO: Analytics:
        //Log Views per User per Date per Page.
        //Not intended to replace Google Analytics or similar. Just to store Views for each blog.
        public int ID { get; set; }
        public string IPAddress { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
        public string Username { get; set; }

        public UniqueView()
        {
            Count = 0;
        }
    }
}
