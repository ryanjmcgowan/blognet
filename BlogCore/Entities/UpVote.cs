﻿using System;

namespace BlogCore.Entities
{
    public class UpVote : BaseContent
    {
        public int ID { get; set; }
        public bool IsUp { get; set; }
        public IUser User { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}