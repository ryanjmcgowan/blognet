﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlogCore.Entities;

namespace BlogData
{
    public class AuthorRepository : BaseRepository<Author>
    {
        private BlogContext db;

        public AuthorRepository(BlogContext context)
            : base(context)
        {
            this.db = context;
        }

        public Author GetByUserName(string userName)
        {
            return db.Authors.Where(a => a.UserName == userName).FirstOrDefault();
        }

    }
}