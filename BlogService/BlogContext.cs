﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using BlogCore.Entities;
using System.Configuration;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace BlogData
{
    public class BlogContext : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<BlackListWord> BlackListedWords { get; set; }

        public BlogContext()
            : base(ConfigurationManager.ConnectionStrings["BlogDBConnection"].ConnectionString)
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AddressComponentMap());
            modelBuilder.Configurations.Add(new AuthorComponentMap());
            modelBuilder.Configurations.Add(new BlackListWordComponentMap());
            modelBuilder.Configurations.Add(new CategoryComponentMap());
            modelBuilder.Configurations.Add(new CommentsComponentMap());
            modelBuilder.Configurations.Add(new ContactInfoComponentMap());
            modelBuilder.Configurations.Add(new EmailComponentMap());
            modelBuilder.Configurations.Add(new PostComponentMap());
            modelBuilder.Configurations.Add(new ScheduleComponentMap());
            modelBuilder.Configurations.Add(new TagComponentMap());
        }
        
    }

    public class AddressComponentMap : EntityTypeConfiguration<Address>
    {
        public AddressComponentMap()
        {
            this.Map(a => a.MapInheritedProperties()).ToTable("Addresses");
        }
        
    }

    public class AuthorComponentMap : EntityTypeConfiguration<Author>
    {
        public AuthorComponentMap()
        {
            this.Map(a => a.MapInheritedProperties()).ToTable("Authors");
            this.HasMany(a => a.ContactInfo).WithRequired();
        }
    }

    public class BlackListWordComponentMap : EntityTypeConfiguration<BlackListWord>
    {
        public BlackListWordComponentMap()
        {
            this.Map(b => b.MapInheritedProperties()).ToTable("BlackListWords");
            this.Property(b => b.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }

    public class CategoryComponentMap : EntityTypeConfiguration<Category>
    {
        public CategoryComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("Categories");
        }
    }

    public class CommentsComponentMap : EntityTypeConfiguration<Comment>
    {
        public CommentsComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("Comments");
            this.HasRequired(c => c.Post).WithMany();
        }
    }

    public class ContactInfoComponentMap : EntityTypeConfiguration<ContactInfo>
    {
        public ContactInfoComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("ContactInfo");
        }
    }

    public class EmailComponentMap : EntityTypeConfiguration<Email>
    {
        public EmailComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("Emails");
            this.HasRequired(e => e.Contact).WithRequiredPrincipal();
        }
    }

    public class PostComponentMap : EntityTypeConfiguration<Post>
    {
        public PostComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("Posts");
            this.HasKey(m => m.ID);
            this.HasMany(p => p.Categories).WithMany();
            this.HasMany(p => p.Tags).WithMany();
            this.HasMany(p => p.UpVotes);
            this.HasRequired(p => p.Author);
            this.HasRequired(p => p.Schedule);
            this.HasMany(p => p.UniqueViews).WithRequired();
        }
    }

    public class ScheduleComponentMap : EntityTypeConfiguration<Schedule>
    {
        public ScheduleComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("Schedules");
            this.Property(m => m.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }

    public class TagComponentMap : EntityTypeConfiguration<Tag>
    {
        public TagComponentMap()
        {
            this.Map(m => m.MapInheritedProperties()).ToTable("Tags");
        }
    }
}
