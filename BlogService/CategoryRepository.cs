﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BlogCore.Entities;

namespace BlogData
{
    public class CategoryRepository : BaseRepository<Category>
    {
        private BlogContext db;

        public CategoryRepository(BlogContext context) : base(context)
        {
            this.db = context;
        }

        public override bool Delete(int id)
        {
            Category c = db.Categories.Find(id);
            if (c == null)
                throw new NullReferenceException("Category not found.");
            c.IsDeleted = true;
            if (db.SaveChanges() == 1)
                return true;
            return false;
        }

        public Category GetByName(string name)
        {
            return db.Categories.Where(c => c.Name == name).FirstOrDefault();
        }
 
    }
}