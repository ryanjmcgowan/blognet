﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlogCore.Entities;

namespace BlogData
{
    public class CommentRepository : BaseRepository<Comment>
    {
        private BlogContext db;

        public CommentRepository(BlogContext context)
            : base(context)
        {
            this.db = context;
        }
    }
}