﻿using System.Linq;
using BlogCore.Entities;

namespace BlogData
{
    interface IRepository<TEntity> where TEntity:BaseContent
    {
        IQueryable<TEntity> GetAll();
        TEntity GetByID(int id);
        TEntity Insert(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(int id);
        int SaveChanges();
    }
}
