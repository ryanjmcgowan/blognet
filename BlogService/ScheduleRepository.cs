﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlogCore.Entities;

namespace BlogData
{
    public class ScheduleRepository : BaseRepository<Schedule>
    {
        private BlogContext db;

        public ScheduleRepository(BlogContext context)
            : base(context)
        {
            this.db = context;
        }
    }
}