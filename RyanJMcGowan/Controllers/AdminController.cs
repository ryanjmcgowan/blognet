﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BlogCore.Entities;
using RyanJMcGowan.ViewModels;
using BlogData;
using System.Data.Entity;
using System.Data.Entity.Core;
using NLog;
using System.Linq;
using System.Data.SqlClient;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Data;
using System.Text.RegularExpressions;

namespace RyanJMcGowan.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            //TODO: Menu to all the management pages
            return View();
        }

        //------------Posts-------------
        public ActionResult ManagePosts()
        {
            try
            {
                //TODO: Get by author, unless admin. Then:
                List<Post> posts = context.Posts.Take(10).ToList();
                return View(posts);
            }
            catch (DataException e)
            {
                ModelState.AddModelError("", "There was an error connecting to the database. Please try again later.");
            }
            return View(new List<Post>());
        }

        public ActionResult EditPost(int? id)
        {
            Post post;
            if (id == null)
            {
                Author author = authorDB.Where(a => a.UserName == User.Identity.Name).SingleOrDefault();
                post = new Post(author, new Schedule());

                context.Entry(post).State = EntityState.Added;
                context.SaveChanges();
            }
            post = postDB.Include(p => p.Schedule).Single(p => p.ID == id);
            EditPostModel model = new EditPostModel(post);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditPost(EditPostModel editPost)
        {
             //Make sure the user is the author.
            Post post;
            try
            {
                post = postDB.Where(p => p.ID == editPost.ID).Include(p => p.Author).FirstOrDefault();
                if (post != null && post.Author.Name == User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest,"The post you are trying to edit does not belong to you.");
                }
                //User is legit author. Update post.
                if (ModelState.IsValid)
                {
                    Author author = authorDB.Where(a => a.UserName == User.Identity.Name).SingleOrDefault();
                    Schedule schedule = scheduleDB.Where(s => s.ID == editPost.ID).SingleOrDefault();
                    schedule.PublishDate = editPost.PublishDate;

                    // if this post does not exist yet, create a new one.
                    post.Author = author;//TODO: Categories
                    post.Subheading = editPost.Subheading;
                    post.Content = editPost.Content;
                    post.Title = editPost.Title;
                    post.Schedule = schedule;
                    post.IsVisible = editPost.IsVisible;

                    context.Schedules.Attach(schedule);
                    context.Entry(post).State = EntityState.Modified;
                    context.SaveChanges();
                    return RedirectToAction("ManagePosts");
                }
            }
            catch (DataException)
            { }
            return View(editPost);
        }

        [HttpPost]
        public ActionResult DeletePost(int id)
        {
            try
            {

            }
            catch (DataException)
            { }
            Response.StatusCode = 204;
            return View(new EditPostModel());
        }

        [HttpGet]
        public ActionResult Preview(int id)
        {
            try
            {
                PostModel model = new PostModel(postDB.Find(id));
                return View(model);
            }
            catch (DataException)
            {
                ViewBag.Error = "There was a problem with the database. An error has been logged.";
            }
            return View();
        }
        
        //------------Authors----------------
        public ActionResult ManageAuthors()
        {
            //TODO ManageAuthorViewModel

            List<Author> authors = context.Authors.Include(a => a.ContactInfo.Select(c => c.Email)).ToList();
            List<AuthorsViewModel> model = new List<AuthorsViewModel>();
            foreach (Author a in authors)
            {
                model.Add(AuthorsViewModel.FromAuthor(a));
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ManageAuthors(int id,object author)
        {
            //TODO ManageAuthorViewModel object input IEnumerable
            return View();
        }

        public ActionResult CreateAuthor()
        {
            //TODO 
            return View(new AuthorsViewModel());
        }

        [HttpPost]
        public ActionResult CreateAuthor(AuthorsViewModel author)
        {
            //TODO [HttpPut] CreateAuthor(AuthorsViewModel author)
            AuthorsViewModel.Validate(author);

            Author record = new Author();
            
            DateTime now = DateTime.Now;
            record.Name = author.Name;
            try
            {
                authorDB.Add(record);
            }
            catch (EntityException e)
            {
                logger.ErrorException("CreateAuthor failed. Input: " + author.ToString() + " Exception: " + e.Message, e);
            }
            catch (InvalidOperationException e)
            {
                logger.ErrorException("CreateAuthor failed. Input: " + author.ToString() + " Exception: " + e.Message, e);
            }
            return View(author);
        }

        [HttpDelete]
        public ActionResult DisableAuthor(int id)
        {
            //disable all of author's posts.
            Response.StatusCode = 204;
            return View();
        }

        [HttpGet]
        public ActionResult EditAuthor(int id)
        {
            //Response.StatusCode = 204;
            try
            {
                return View(AuthorsViewModel.FromAuthor(authorDB.Where(a => a.ID == id).FirstOrDefault()));
            }
            catch (DataException)
            {
                ViewBag.Error = "There was an error finding the author in the database. Try again later or check your input.";
            }
            return View();
        }

        //------------Comments-----------
        public ActionResult ManageComments(int post)
        {
            //TODO Get Comments for Post.ID = post
            return View(new List<Comment>());
        }

        [HttpPost]
        public ActionResult DeleteComments(int[] commentIDs)
        {
            //TODO Get Comments for Post.ID = post
            Response.StatusCode = 204;
            return View("ManageComments", new List<Comment>());
        }

        public ActionResult ManageSchedule(int post)
        {
            //TODO Get Schedule for Post.ID = post
            return View();
        }

        public ActionResult Seed()
        {
            //Address
            Address address1 = new Address();
            address1.Street1 = "1441 Main Street";
            address1.City = "Somecity";
            address1.State = "State";
            address1.Zip = "12345";
            address1.IsMailing = true;

            //ContactInfo
            ContactInfo contact1 = new ContactInfo("ryan@email.com", "7602154493", address1, ContactType.Home);
            
            //Authors
            Author author1 = new Author();
            author1.SetPrimaryContactInfo(contact1);
            
            author1.Name = "Ryan McGowan";
            author1.UserName = "ryanjmcgowan";

            //Categories
            Category category1 = new Category();
            category1.Name = "Articles";
            Category category2 = new Category();
            category2.Name = "News";
            Category category3 = new Category();
            category3.Name = "Reviews";
            Category category4 = new Category();
            category4.Name = "Reviews";
            //Schedules

            //Posts

            //Comments

            context.Authors.Add(author1);

            context.SaveChanges();
            return View("Index");
        }
        
        //TODO: Backup old versions of posts in a read-only manner.
        //TODO: Allow authors to change usernames without messing up validation logic.
        //

        private static BlogContext context = new BlogContext();
        private DbSet<Post> postDB = context.Set<Post>();
        private DbSet<Author> authorDB = context.Set<Author>();
        private DbSet<Schedule> scheduleDB = context.Set<Schedule>();
        private DbSet<Category> categoryDB = context.Set<Category>();
        private DbSet<Comment> commentDB = context.Set<Comment>();

        private static Logger logger = LogManager.GetCurrentClassLogger();
    }
}
