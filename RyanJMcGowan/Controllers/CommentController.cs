﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RyanJMcGowan.Controllers
{
    public class CommentController : ApiController
    {
        // GET api/comment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/comment/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/comment
        public void Post([FromBody]string value)
        {
            //TODO: Filter comments for spam before post.
        }

        // PUT api/comment/5
        public void Put(int id, [FromBody]string value)
        {
            //TODO: Keep old version. Replace old comment. Activate "Edited" link to view revision history.
        }

        // DELETE api/comment/5
        public void Delete(int id)
        {
            //TODO Delete comment.
        }
    }
}
