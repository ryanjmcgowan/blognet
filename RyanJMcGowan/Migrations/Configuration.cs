namespace RyanJMcGowan.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using RyanJMcGowan.Models;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<RyanJMcGowan.Models.UsersContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RyanJMcGowan.Models.UsersContext context)
        {
            //Seed admin user.
            WebSecurity.InitializeDatabaseConnection("BlogDBConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            if (!Roles.RoleExists("Administrator"))
                Roles.CreateRole("Administrator");
            if (!WebSecurity.UserExists("lelong37"))
                WebSecurity.CreateUserAndAccount("ryanjmcgowan", "power09234");
            if (!Roles.GetRolesForUser("ryanjmcgowan").Contains("Administrator"))
                Roles.AddUsersToRoles(new[] { "ryanjmcgowan" }, new[] { "Administrator" });

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //


        }
    }
}
