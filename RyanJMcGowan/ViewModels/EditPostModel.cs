﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogCore.Entities;

namespace RyanJMcGowan.ViewModels
{
    public class EditPostModel : BaseContent, IPost
    {
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        [StringLength(200), Required]
        public string Subheading { get; set; }
        [StringLength(12)]
        public string FeatureImageKey { get; set; }
        [Display(Name = "Publish Date"),Required]
        public DateTime PublishDate { get; set; }
        [Display(Name = "Visible")]
        public bool IsVisible { get; set; }
        private Author author { get; set; }

        public EditPostModel()
        {

        }

        public EditPostModel(Post post)
        {
            Title = post.Title;
            Subheading = post.Subheading;
            Content = post.Content;
            FeatureImageKey = post.FeatureImageKey;
            PublishDate = post.Schedule.PublishDate;
            IsVisible = post.IsVisible;
            author = post.Author;
        }
        internal bool IsValidated()
        {
            //TODO: EditPostModel.IsValidated()
            return true;
        }

        public Post ToPost(Author author, bool edited, List<Category> category)
        {
            // Populate Post
            Post post = new Post();
            post.Author = author;
            post.Content = this.Content;
            post.Title = this.Title;
            post.FeatureImageKey = this.FeatureImageKey;
            post.URL = this.GetURL();
            post.FeatureImageKey = "null";

            // Populate Subobjects
            post.Tags = new List<Tag>();
            post.UpVotes = new List<UpVote>();
            post.UniqueViews = new List<UniqueView>();
            post.Author = author;
            post.Categories = category;
            Schedule schedule = new Schedule();

            schedule.PublishDate = this.PublishDate;
            schedule.ShareOnFacebook = false;
            schedule.ShareOnGooglePlus = false;
            schedule.ShareOnInstagram = false;
            schedule.ShareOnLinkedIn = false;
            schedule.ShareOnMeetup = false;
            schedule.ShareOnMySpace = false;
            schedule.ShareOnPintrest = false;
            schedule.ShareOnTwitter = false;
            post.Schedule = schedule;
            return post;
        }

        public string GetURL()
        {
            string result = new string(this.Title.Where(c => char.IsLetterOrDigit(c)).ToArray());
            result.Replace(" ", string.Empty);
            return result;
        }
    }
}