﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlogCore.Entities;

namespace RyanJMcGowan.ViewModels
{
    public class AuthorsViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ContactType ContactType { get; set; }
        public SmartList<Address> Addresses { get; set; }
        public Phone PrimaryPhoneNumber { get; set; }
        public Phone SecondaryPhoneNumber { get; set; }
        public SmartList<Email> Emails { get; set; }
        public string TwitterCallsign { get; set; }
        public IEnumerable<Post> Posts { get; set; }

        public AuthorsViewModel()
        {
            Addresses = new SmartList<Address>();
            Emails = new SmartList<Email>();
        }
        internal static void Validate(AuthorsViewModel author)
        {
            //TODO:
            return;
        }

        public override string ToString()
        {
            return "Name: " + Name
                + " TwitterCallSign: " + TwitterCallsign + " ";
        }

        internal static AuthorsViewModel FromAuthor(Author author)
        {
            AuthorsViewModel result = new AuthorsViewModel();
            result.ID = author.ID;
            result.Name = author.Name;
            SmartList<Address> addresses = new SmartList<Address>();
            SmartList<Email> emails = new SmartList<Email>();
            foreach (ContactInfo c in author.ContactInfo)
            {
                addresses.Add(c.Address);
                emails.Add(c.Email);
            }
            result.Emails = (SmartList<Email>)emails;
            result.Addresses = (SmartList<Address>)addresses;
            return result;
        }
    }
}